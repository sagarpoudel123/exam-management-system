admitparams = {
    examDetail: {
        _id: "6478bae622238dd1cc350dca",
        name: "Final Term",
        description: "internal exam",
        department: {
            _id: "646df6ba917850db2735e9eb",
            name: "Civil Engineering"
        },
        semester: {
            _id: "6473571e0ee067328b63df8c",
            name: "I"
        },
        section: [
            {
                _id: "647358aa0ee067328b63e153",
                name: "A"
            },
            {
                _id: "6473576a0ee067328b63dfea",
                name: "B"
            }
        ],
        year: "2023-2024",
        students: [
            {
                _id: "6473520e0ee067328b63dc47",
                name: "stasa"
            },
            {
                _id: "6473523f0ee067328b63dc71",
                name: "nfaj"
            }
        ],
        createdAt: "2023-06-01T15:36:06.695Z",
        updatedAt: "2023-06-01T15:36:06.695Z",
        __v: 0
    },
    // status: true,
    // msg: "Exam fetched By Id successfully"
    schedule: [
            {
                _id: "647c95b1dbf785deb36d327e",
                parent_exam: {
                    _id: "6478bae622238dd1cc350dca",
                    name: "Final Term"
                },
                subject: {
                    _id: "647357a50ee067328b63e029",
                    title: "Math IV",
                    credit_hour: 3,
                    course_code: "MTH212"
                },
                has_practical: false,
                date: "2023-06-04T13:45:57.928Z",
                starting_time: {
                    hour: 4,
                    minute: 3,
                    amPm: "AM",
                    _id: "647c95b1dbf785deb36d327f"
                },
                ending_time: {
                    hour: 3,
                    minute: 2,
                    amPm: "PM",
                    _id: "647c95b1dbf785deb36d3280"
                },
                fullMarks: 100,
                passMarks: 45,
                place: "Library Block 212",
                createdAt: "2023-06-04T13:46:25.091Z",
                updatedAt: "2023-06-04T13:46:25.091Z",
                __v: 0
            },
            {
                _id: "647c95b8dbf785deb36d32a0",
                parent_exam: {
                    _id: "6478bae622238dd1cc350dca",
                    name: "Final Term"
                },
                subject: {
                    _id: "6445427a69a479bc7e02590d",
                    title: "Chemistry",
                    credit_hour: 4,
                    course_code: "CHM111"
                },
                has_practical: false,
                date: "2023-06-05T13:45:57.000Z",
                starting_time: {
                    hour: 4,
                    minute: 3,
                    amPm: "AM",
                    _id: "647c95b8dbf785deb36d32a1"
                },
                ending_time: {
                    hour: 3,
                    minute: 2,
                    amPm: "PM",
                    _id: "647c95b8dbf785deb36d32a2"
                },
                fullMarks: 100,
                passMarks: 45,
                place: "Library Block 212",
                // "createdAt": "2023-06-04T13:46:32.956Z",
                // "updatedAt": "2023-06-04T13:46:32.956Z",
                // "__v": 0
            },
           
        ],
        // "status": true,
        // "msg": "Timetable fetched by Exam ID successfully"
    }
