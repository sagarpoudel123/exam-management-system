const mongoose = require('mongoose');

const courseAssignmentSchema = new mongoose.Schema({
    course: { type: mongoose.Schema.Types.ObjectId, ref: 'Course', required: true },
    teacher: { type: mongoose.Schema.Types.ObjectId, ref: 'Teacher', required: true }
  }); 

const sectionSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  courseAssignments:[courseAssignmentSchema],
  students: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Student'
  }]
});

const semesterSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    courses: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Course'
    }],
    sections: [sectionSchema]
  });

const departmentSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    hod: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Teacher',
      required: true
    },
    semesters: [semesterSchema]
  });

const Department = mongoose.model('DepartmentSK', departmentSchema);

module.exports = Department;
