import React, { useEffect, useRef } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import { useReactToPrint } from 'react-to-print'
import "../../assets/css/admitcard.css";
import logo from "../../assets/image/png/logo-no-background.png"
import photo from "../../assets/image/png/logo-black.png"
function AdmitCard() {
    const location = useLocation();
    const admitparams = location.state?.admitparams;
    let section = "";
    if (admitparams.exam.section) {
        section = admitparams.exam.section.map(item => item.name).join(',');
    }

    console.log(admitparams)
    const componentRef = useRef();

    const handlePrint = useReactToPrint({
        content: () => componentRef.current,
        documentTitle: 'Admit Card',
        onAfterPrint: () => alert('Print successfully')
    })

    const sectionMap = {};
    if (admitparams) {

        admitparams.exam.section.forEach(section => {
            sectionMap[section._id] = section.name;
        });
    }

    return (
        admitparams && <>

            <Row>
                <Col sm={9}>
                    <h3 className='text-center'>{`Admit cards for  ${admitparams.exam.department.name} Department - ${admitparams.exam.semester.name} Semester - Section: ${section}`} </h3>
                </Col>
                <Col sm={3}>

                    <button
                        type="button"
                        className="btn btn-lg mt-1"
                        onClick={handlePrint}
                        style={{
                            backgroundColor: "#27ae60",
                            color: "#fff",
                            fontSize: "1rem",
                            padding: "0.2rem 1.5rem",
                            borderRadius: "0.5rem",
                            position: "fixed",
                            top: "13",
                            right: "10px",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        <i className="fas fa-print" style={{ marginRight: "0.5rem" }}></i>
                        Print
                    </button>
                </Col>
            </Row>

            {admitparams.exam.students && admitparams.exam.students.map((item, index) => (


                <section key={index} ref={componentRef} style={{ width: '100%', height: window.innerHeight }}>
                    <div className="container">
                        <div className="admit-card mt-3">
                            <div className="row">
                                <div className="col-8 d-flex flex-column justify-content-end align-items-end">
                                    <img style={{ marginRight: '60px', marginBottom: '5px' }} src={logo} width="120px" alt="Mewar University" />
                                    <span style={{ marginRight: '-30px', fontFamily: "Arial, sans-serif", fontSize: "1.5rem", fontWeight: "bold", color: "#333" }}>
                                        Nepal Engineering College
                                    </span>
                                    <p style={{ fontFamily: "Arial, sans-serif", fontSize: "1rem", color: "#777", marginRight: '10px' }}>
                                        Changunarayan, Bhaktapur, Nepal
                                    </p>
                                </div>

                                <div className="col-4 d-flex justify-content-end align-items-end">
                                    <img src={photo} width="150px" alt="Mewar University" />
                                </div>
                            </div>


                            <h3 className="text-center" style={{ fontFamily: "Arial, sans-serif", fontSize: "2rem", fontWeight: "bold", color: "#333" }}>
                                Entrance Card
                            </h3>



                            {/* upper section complete */}
                            <div className="row">
                                <div className="col-sm-6 mt-2">
                                    <span style={{ color: "#007bff", fontWeight: "bold" }}>Exam Roll Number:</span>
                                    <span style={{ color: "red", fontWeight: "bold" }}>20070167</span><br />
                                    <span style={{ color: "#007bff", fontWeight: "bold" }}>Name of the Student:</span>
                                    <span style={{ color: "#333", fontWeight: "bold" }}>{item.name}</span><br />
                                    <span style={{ color: "#007bff", fontWeight: "bold" }}>Department:</span>
                                    <span style={{ color: "#333", fontWeight: "bold" }}>{admitparams.exam.department.name}</span><br />
                                    <span style={{ color: "#007bff", fontWeight: "bold" }}>Semester:</span>
                                    <span style={{ color: "#333", fontWeight: "bold" }}>{admitparams.exam.semester.name}</span><br />
                                    <span style={{ color: "#007bff", fontWeight: "bold" }}>Section:</span>
                                    <span style={{ color: "#333", fontWeight: "bold" }}>
                                        {sectionMap[item.section] || ''}
                                    </span>
                                </div>
                            </div>


                            <div className="row mt-2">
                                <div className="col-sm-12">
                                    <h4 className="text-center" style={{ fontFamily: "Arial, sans-serif", fontSize: "1.5rem", fontWeight: "bold", color: "#333333" }}>Schedule for {admitparams.exam.name}</h4>



                                    <table className="table table-bordered" style={{ fontFamily: "Arial, sans-serif", fontSize: "1rem" }}>
                                        <thead style={{ backgroundColor: "#f8f9fa" }}>
                                            <tr>
                                                <th style={{ color: "#333333" }}>Sr. No.</th>
                                                <th style={{ color: "#333333" }}>Exam Date</th>
                                                <th style={{ color: "#333333" }}>Course Code</th>
                                                <th style={{ color: "#333333" }}>Course Title</th>
                                                <th style={{ color: "#333333" }}>Credit Hour</th>
                                                <th style={{ color: "#333333" }}>Starting Time</th>
                                                <th style={{ color: "#333333" }}>Ending Time</th>
                                                <th style={{ color: "#333333" }}>Room/Block</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {admitparams.schedule && admitparams.schedule.map((value, index) => (
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td>{value.date.split('T')[0]}</td>
                                                    <td>{value.subject.course_code}</td>
                                                    <td>{value.subject.title}</td>
                                                    <td>{value.subject.credit_hour}</td>
                                                    <td>{`${value.starting_time.hour}:${value.starting_time.minute}:${value.starting_time.amPm}`}</td>
                                                    <td>{`${value.ending_time.hour}:${value.ending_time.minute}:${value.ending_time.amPm}`}</td>
                                                    <td>{value.place}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                    <h5 style={{ fontFamily: "Arial, sans-serif", fontSize: "1.2rem", fontWeight: "bold", color: "#333333", marginTop: "1.5rem" }}>
                                        INSTRUCTIONS:
                                    </h5>
                                    <div className="row">
                                        <div className="col" style={{ fontFamily: "Arial, sans-serif", fontSize: "1rem", color: "#333333" }} dangerouslySetInnerHTML={{ __html: admitparams.exam.description }}>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <footer className="txt-center">
                                <hr />
                            </footer>
                        </div>
                    </div>
                </section>))}
        </>
    );
}

export default AdmitCard;
